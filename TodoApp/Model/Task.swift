//
//  Task.swift
//  TodoApp
//
//  Created by aluno on 04/07/20.
//  Copyright © 2020 cesar. All rights reserved.
//
import UIKit

class Task: NSObject, NSCoding {
    var name: String
    var category: String
    var taskDate: String
    var activitys: Array<Activity>
    
    init(name: String, category: String, taskDate: String, activitys: Array<Activity>){
        self.name = name
        self.category = category
        self.taskDate = taskDate
        self.activitys = activitys
    }
    
    override init(){
        self.name = ""
        self.category = ""
        self.taskDate = ""
        self.activitys = Array<Activity>()
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        self.category = aDecoder.decodeObject(forKey: "category") as? String ?? ""
        self.taskDate = aDecoder.decodeObject(forKey: "taskDate") as? String ?? ""
        self.activitys = aDecoder.decodeObject(forKey: "activitys") as? Array<Activity> ?? Array<Activity>()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(category, forKey: "category")
        aCoder.encode(taskDate, forKey: "taskDate")
        aCoder.encode(activitys, forKey: "activitys")
    }
}
