//
//  Activity.swift
//  TodoApp
//
//  Created by aluno on 04/07/20.
//  Copyright © 2020 cesar. All rights reserved.
//
import UIKit

class Activity: NSObject, NSCoding {
    var name: String
    var isActive: Bool
    
    init(name: String, isActive: Bool){
        self.name = name
        self.isActive = isActive
    }
    
    required init?(coder aDecoder: NSCoder) {
        self.name = aDecoder.decodeObject(forKey: "name") as? String ?? ""
        self.isActive = aDecoder.decodeBool(forKey: "isActive")
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(isActive, forKey: "isActive")
    }
}
