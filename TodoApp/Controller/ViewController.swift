//
//  TesteTableTableViewController.swift
//  TodoApp
//
//  Created by aluno on 06/07/20.
//  Copyright © 2020 cesar. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var roundButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurarRoundButton()
        loadTasks()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    func loadTasks(){
        
        let userDefaults = UserDefaults.standard
        var tasks = Array<Task>()
        if let taskData = userDefaults.object(forKey: "tasks") as? NSData {
            do {
                tasks = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(taskData as Data) as! [Task]
            } catch {
                print("Um erro ocorreu ao recuperar o UserDefault")
            }
        }
        
        if tasks.count == 0 {
            let diarias = [Activity(name: "Limpar a casa", isActive: false), Activity(name: "Estudar", isActive: false)]
            let urgentes = [Activity(name: "Trabalhar", isActive: false), Activity(name: "Fazer a Feira", isActive: false)]
            let normais = [Activity(name: "Varrer", isActive: false), Activity(name: "Cozinhar", isActive: false)]
            let agua = [Activity(name: "Beber as 10:00", isActive: false), Activity(name: "Beber as 14:00", isActive: false)]
            
            tasks = [Task(name: "Atividade Diaria", category: "daily", taskDate: "as 8:40", activitys: diarias),
                         Task(name: "Demandas Urgentes", category: "priority", taskDate: "as 12:57", activitys: urgentes),
                         Task(name: "Casa", category: "home", taskDate: "as 15:22", activitys: normais),
                         Task(name: "Agua", category: "water", taskDate: "a cada 1 hora", activitys: agua)]
            
            do {
                let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: tasks, requiringSecureCoding: false)
                userDefaults.set(encodedData, forKey: "tasks")
                userDefaults.synchronize()
            } catch {
                print("Ocorreu um erro")
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.tableView.reloadData()
    }
        
    @objc func novaTask(_ sender: UIButton){
        print("Nova Task Aqui!")
        let create = AddTaskViewController(nibName: "AddTaskViewController", bundle: nil)
        navigationController?.pushViewController(create, animated: true)
    }
    
    @objc func editarTask(_ sender: UIButton){
        // use the tag of button as index
        let defaults = UserDefaults.standard
        var tasks = Array<Task>()
        if let taskData = defaults.object(forKey: "tasks") as? NSData {
            do {
                tasks = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(taskData as Data) as! [Task]
            } catch {
                print("Um erro ocorreu ao recuperar o UserDefault")
            }
        }
        
        print("Editar Task Aqui!")
        print("\(tasks[sender.tag].name)")
        
        let create = EditTaskViewController(nibName: "EditTaskViewController", bundle: nil)
        create.task = tasks[sender.tag]
        create.posicao = sender.tag
        navigationController?.pushViewController(create, animated: true)
        
    }
    
    func configurarRoundButton(){
        roundButton.addTarget(self, action: #selector(novaTask), for: .touchUpInside)
        roundButton.layer.cornerRadius = roundButton.layer.frame.size.width/2
        roundButton.clipsToBounds = true
        roundButton.translatesAutoresizingMaskIntoConstraints = false
        
        roundButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20).isActive = true
        roundButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -53).isActive = true
        roundButton.widthAnchor.constraint(equalToConstant: 50).isActive = true
        roundButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }

    
    @IBAction func actionSheet(_ sender: Any) {
        
        let optionMenu = UIAlertController(title: nil, message: "Opções", preferredStyle: .actionSheet)
            
        let saveAction = UIAlertAction(title: "Listar por Categorias", style: .default) { action -> Void in
            let viewController = CategoryViewController(nibName: "CategoryViewController", bundle: nil)
            
            var currentNav = self.navigationController?.viewControllers
            currentNav?.removeAll()
            currentNav?.append(viewController)
            
            self.navigationController?.setViewControllers(currentNav!, animated: true)
        }
            
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel)
            
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
            
        self.present(optionMenu, animated: true, completion: nil)
    }
    
}

extension ViewController: UITableViewDelegate {
    
}

extension ViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let defaults = UserDefaults.standard
        var tasks = Array<Task>()
        if let taskData = defaults.object(forKey: "tasks") as? NSData {
            do {
                tasks = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(taskData as Data) as! [Task]
            } catch {
                print("Um erro ocorreu ao recuperar o UserDefault")
            }
        }

        return tasks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! TableViewCell
        
        cell.editTask.tag = indexPath.row
        cell.editTask.addTarget(self, action: #selector(editarTask), for: .touchUpInside)

        // Configure the cell...
        let defaults = UserDefaults.standard
        var tasks = Array<Task>()
        if let taskData = defaults.object(forKey: "tasks") as? NSData {
            do {
                tasks = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(taskData as Data) as! [Task]
            } catch {
                print("Um erro ocorreu ao recuperar o UserDefault")
            }

        }
        let task = tasks[indexPath.row]
        cell.prepare(with:task)
        return cell
    }
    
    // Override to support editing the table view.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            //            tableView.deleteRows(at: [indexPath], with: .fade)
            let defaults = UserDefaults.standard
            var tasks = Array<Task>()
            if let taskData = defaults.object(forKey: "tasks") as? NSData {
                do {
                    tasks = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(taskData as Data) as! [Task]
                } catch {
                    print("Um erro ocorreu ao recuperar o UserDefault")
                }
            }

            tasks.remove(at: indexPath.row)

            do {
                let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: tasks, requiringSecureCoding: false)
                defaults.set(encodedData, forKey: "tasks")
                defaults.synchronize()
            } catch {
                print("Ocorreu um erro")
            }

            self.tableView.reloadData()
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    
    
}
