//
//  EditTaskViewController.swift
//  TodoApp
//
//  Created by aluno on 04/07/20.
//  Copyright © 2020 cesar. All rights reserved.
//

import UIKit

class EditTaskViewController: UIViewController {

    @IBOutlet weak var categoria: UILabel!
    @IBOutlet weak var atividade: UILabel!
    @IBOutlet weak var periodo: UILabel!
    @IBOutlet weak var botaoExcluir: UIButton!
    @IBOutlet weak var leftView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    var task : Task = Task()
    var posicao: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        botaoExcluir.addTarget(self, action: #selector(excluirTask), for: .touchUpInside)
        carregarTask()
    }
    
    @objc func excluirTask(_sender: UIButton){
        let refreshAlert = UIAlertController(title: "Atenção!", message: "Deseja realmente Apagar essa Task?.", preferredStyle: UIAlertController.Style.alert)

        refreshAlert.addAction(UIAlertAction(title: "Sim", style: .default, handler: { (action: UIAlertAction!) in
              let defaults = UserDefaults.standard
              var tasks = Array<Task>()
              if let taskData = defaults.object(forKey: "tasks") as? NSData {
                  do {
                      tasks = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(taskData as Data) as! [Task]
                  } catch {
                      print("Um erro ocorreu ao recuperar o UserDefault")
                  }
              }
              
            tasks.remove(at: self.posicao)
              
              do {
                  let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: tasks, requiringSecureCoding: false)
                  defaults.set(encodedData, forKey: "tasks")
                  defaults.synchronize()
              } catch {
                  print("Ocorreu um erro")
              }
            
            self.navigationController?.popViewController(animated: true)
        }))

        refreshAlert.addAction(UIAlertAction(title: "Não", style: .cancel, handler: { (action: UIAlertAction!) in
              print("Não excluiu a task")
        }))

        present(refreshAlert, animated: true, completion: nil)
    }
    
    func carregarTask(){
        
        categoria.text = task.category.uppercased()
        atividade.text = task.name
        periodo.text = task.taskDate
        
        switch task.category {
        case "daily":
            leftView.backgroundColor = UIColor(named: "daily")
            categoria.textColor = UIColor(named: "daily")
        case "home":
            leftView.backgroundColor = UIColor(named: "home")
            categoria.textColor = UIColor(named: "home")
        case "priority":
            leftView.backgroundColor = UIColor(named: "priority")
            categoria.textColor = UIColor(named: "priority")
        case "water":
            leftView.backgroundColor = UIColor(named: "water")
            categoria.textColor = UIColor(named: "water")
        default:
            leftView.backgroundColor = UIColor(named: "daily")
            categoria.textColor = UIColor(named: "daily")
        }
        
        let stackView = UIStackView()
        
        for (index, activity) in task.activitys.enumerated() {
            let cbAtividade = UIButton()
            cbAtividade.tag = index
            cbAtividade.setTitle(activity.name, for: .normal)
            if(activity.isActive){
                cbAtividade.setImage(UIImage(systemName: "checkmark.square.fill"), for: .normal)
                cbAtividade.tintColor = UIColor(named: "daily")
                cbAtividade.isSelected = true
            } else {
                cbAtividade.setImage(UIImage(systemName: "checkmark.square"), for: .normal)
                cbAtividade.tintColor = UIColor(named: "textoCinza")
                cbAtividade.isSelected = false
            }
            
            cbAtividade.addTarget(self, action: #selector(marcarAtividade), for: .touchUpInside)
            cbAtividade.setTitleColor(UIColor(named: "textoCinza"), for: .normal)
            cbAtividade.translatesAutoresizingMaskIntoConstraints = false
            
            stackView.addArrangedSubview(cbAtividade)
        }
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.distribution = .fill
        stackView.alignment = .leading
        self.scrollView.addSubview(stackView)
        self.scrollView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor).isActive = true
        self.scrollView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor).isActive = true
        self.scrollView.topAnchor.constraint(equalTo: stackView.topAnchor).isActive = true
        self.scrollView.bottomAnchor.constraint(equalTo: stackView.bottomAnchor).isActive = true
        self.scrollView.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
        
    }
    
    @objc func marcarAtividade(_ sender: UIButton){
        if sender.isSelected {
            sender.setImage(UIImage(systemName: "checkmark.square"), for: .normal)
            sender.tintColor = UIColor(named: "textoCinza")
            sender.isSelected = false
            task.activitys[sender.tag].isActive = false
        } else {
            sender.setImage(UIImage(systemName: "checkmark.square.fill"), for: .normal)
            sender.tintColor = UIColor(named: "daily")
            sender.isSelected = true
            task.activitys[sender.tag].isActive = true
        }
        
        atualizarTask()
         
    }
    
    func atualizarTask(){
        let defaults = UserDefaults.standard
        var tasks = Array<Task>()
        if let taskData = defaults.object(forKey: "tasks") as? NSData {
            do {
                tasks = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(taskData as Data) as! [Task]
            } catch {
                print("Um erro ocorreu ao recuperar o UserDefault")
            }
        }
        
        tasks[posicao] = task
        
        do {
            let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: tasks, requiringSecureCoding: false)
            defaults.set(encodedData, forKey: "tasks")
            defaults.synchronize()
        } catch {
            print("Ocorreu um erro")
        }
    }

}
