//
//  CategoryViewController.swift
//  TodoApp
//
//  Created by aluno on 09/07/20.
//  Copyright © 2020 cesar. All rights reserved.
//

import UIKit

class CategoryViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configurarNavBar()
        self.title = "Atividades por Categoria"
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 157, height: 208)
        layout.sectionInset = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 10
        collectionView.collectionViewLayout = layout
        collectionView.register(MyCollectionViewCell.nib(), forCellWithReuseIdentifier: "MyCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
    }

    func configurarNavBar(){
        
        let item = UIBarButtonItem(image: UIImage(systemName: "line.horizontal.3"), style: .plain, target: self, action: #selector(actionSheet))
        self.navigationItem.rightBarButtonItem = item
    }
    

    @objc func actionSheet(_ sender: Any) {
        
        let optionMenu = UIAlertController(title: nil, message: "Opções", preferredStyle: .actionSheet)
            
        let saveAction = UIAlertAction(title: "Listar Atividades", style: .default) { action -> Void in
            guard let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ViewController") as? ViewController else {
                return
            }
            
            var currentNav = self.navigationController?.viewControllers
            currentNav?.removeAll()
            currentNav?.append(viewController)

            self.navigationController?.setViewControllers(currentNav!, animated: true)
        }
            
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel)
            
        optionMenu.addAction(saveAction)
        optionMenu.addAction(cancelAction)
            
        self.present(optionMenu, animated: true, completion: nil)
        
    }
    
}

extension CategoryViewController : UICollectionViewDelegate {
    
}

extension CategoryViewController : UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let defaults = UserDefaults.standard
        var tasks = Array<Task>()
        if let taskData = defaults.object(forKey: "tasks") as? NSData {
            do {
                tasks = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(taskData as Data) as! [Task]
            } catch {
                print("Um erro ocorreu ao recuperar o UserDefault")
            }
        }

        return tasks.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MyCollectionViewCell", for: indexPath) as! MyCollectionViewCell
        
        let defaults = UserDefaults.standard
        var tasks = Array<Task>()
        if let taskData = defaults.object(forKey: "tasks") as? NSData {
            do {
                tasks = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(taskData as Data) as! [Task]
            } catch {
                print("Um erro ocorreu ao recuperar o UserDefault")
            }

        }
        
        cell.task = tasks[indexPath.row]
        cell.posicao = indexPath.row
        cell.prepare()
        
        return cell
    }
    
}

extension CategoryViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 157, height: 208)
    }
}
