//
//  AddEditTaskViewController.swift
//  TodoApp
//
//  Created by aluno on 04/07/20.
//  Copyright © 2020 cesar. All rights reserved.
//

import UIKit

class AddTaskViewController: UIViewController {

    @IBOutlet var container: UIView!
    @IBOutlet weak var etCategoria: UITextField!
    @IBOutlet weak var etNomeAtividade: UITextField!
    @IBOutlet weak var etPeriodo: UITextField!
    @IBOutlet weak var etAtividade: UITextField!
    @IBOutlet weak var btnAdicionar: UIButton!
    @IBOutlet weak var btnSalvar: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    var listCategory = ["daily", "home", "water", "priority"]
    
    var task = Task()
    
    var stackView = UIStackView()
    
    lazy var pickerView: UIPickerView = {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        pickerView.dataSource = self
        pickerView.backgroundColor = .white
        return pickerView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        prepararTela()
    }

    
    func prepararTela(){
        btnAdicionar.layer.cornerRadius = btnAdicionar.layer.frame.size.width / 2
        btnAdicionar.clipsToBounds = true
        btnAdicionar.addTarget(self, action: #selector(adicionarTask), for: .touchUpInside)
        
        let toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: 44))
        toolbar.tintColor = UIColor(named: "main")
        let btCancel = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancel))
        let btDone = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
        let btFlexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        toolbar.items = [btCancel, btFlexibleSpace, btDone]
        
        // tip. faz o text field exibir os dados predefinidos pela picker view
        etCategoria.inputView = pickerView
        etCategoria.inputAccessoryView = toolbar
        
        btnSalvar.layer.cornerRadius = btnSalvar.layer.frame.size.width / 2
        btnSalvar.addTarget(self, action: #selector(salvarTask), for: .touchUpInside)
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.distribution = .fill
        stackView.alignment = .leading
        self.scrollView.addSubview(stackView)
        self.scrollView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor).isActive = true
        self.scrollView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor).isActive = true
        self.scrollView.topAnchor.constraint(equalTo: stackView.topAnchor).isActive = true
        self.scrollView.bottomAnchor.constraint(equalTo: stackView.bottomAnchor).isActive = true
        self.scrollView.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
    }
    
    @objc func cancel() {
        // ocultar teclado
        etCategoria.resignFirstResponder()
    }
    
    @objc func done() {
        etCategoria.text = listCategory[pickerView.selectedRow(inComponent: 0)].uppercased()
        switch listCategory[pickerView.selectedRow(inComponent: 0)] {
        case "daily":
            container.backgroundColor = UIColor(named: "daily")
            btnAdicionar.backgroundColor = UIColor(named: "water")
            btnSalvar.backgroundColor = UIColor(named: "water")
        case "home":
            container.backgroundColor = UIColor(named: "home")
            btnAdicionar.backgroundColor = UIColor(named: "priority")
            btnSalvar.backgroundColor = UIColor(named: "priority")
        case "priority":
            container.backgroundColor = UIColor(named: "priority")
            btnAdicionar.backgroundColor = UIColor(named: "home")
            btnSalvar.backgroundColor = UIColor(named: "home")
        case "water":
            container.backgroundColor = UIColor(named: "water")
            btnAdicionar.backgroundColor = UIColor(named: "daily")
            btnSalvar.backgroundColor = UIColor(named: "daily")
        default:
            container.backgroundColor = UIColor(named: "daily")
        }
        cancel()
    }
    
    fileprivate func alerta(_ mensagem: String) {
        let uialert = UIAlertController(title: "Atenção", message: mensagem, preferredStyle: UIAlertController.Style.alert)
        uialert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))
        self.present(uialert, animated: true, completion: nil)
    }
    
    @objc func adicionarTask(){
        if etAtividade.text != "" {
            let cbAtividade = UIButton()
            cbAtividade.setTitle(etAtividade.text?.lowercased(), for: .normal)
            
            cbAtividade.setTitleColor(UIColor(ciColor: .black), for: .normal)
            cbAtividade.translatesAutoresizingMaskIntoConstraints = false
            
            task.activitys.append(Activity(name: etAtividade.text!, isActive: false))
            etAtividade.text = ""
            
            stackView.addArrangedSubview(cbAtividade)
        } else {
            alerta("Informe o nome da Atividade")
        }
    }
    
    @objc func salvarTask(){
        
        task.name = etNomeAtividade.text!
        task.category = etCategoria.text!.lowercased()
        task.taskDate = etPeriodo.text!
        
        if task.name == "" || task.category == "" || task.taskDate == "" || task.activitys.count == 0 {
               alerta("Preencha todos os campos da tela!")
        } else {
            let defaults = UserDefaults.standard
            var tasks = Array<Task>()
            if let taskData = defaults.object(forKey: "tasks") as? NSData {
                do {
                    tasks = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(taskData as Data) as! [Task]
                } catch {
                    print("Um erro ocorreu ao recuperar o UserDefault")
                }
            }
            
            tasks.append(task)
            
            do {
                let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: tasks, requiringSecureCoding: false)
                defaults.set(encodedData, forKey: "tasks")
                defaults.synchronize()
            } catch {
                print("Ocorreu um erro")
            }
        }
        
        self.navigationController?.popViewController(animated: true)
        
    }
}

extension AddTaskViewController : UIPickerViewDelegate, UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    // UIPickerViewDataSource
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return listCategory.count
    }
    
    // UIPickerViewDelegate
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return listCategory[row].uppercased()
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
    }
    
}
