//
//  MyCollectionViewCell.swift
//  TodoApp
//
//  Created by aluno on 09/07/20.
//  Copyright © 2020 cesar. All rights reserved.
//

import UIKit

class MyCollectionViewCell: UICollectionViewCell {

    var task : Task = Task()
    var posicao: Int = 0
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var categoria: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }   

    static func nib() -> UINib {
        return UINib(nibName: "MyCollectionViewCell", bundle: nil)
    }
    
    func prepare() {
        container.layer.cornerRadius = 10
        container.clipsToBounds = true
        
        switch task.category {
        case "daily":
            container.backgroundColor = UIColor(named: "daily")
            categoria.text = "Daily".uppercased()
        case "home":
            container.backgroundColor = UIColor(named: "home")
            categoria.text = "Home".uppercased()
        case "priority":
            container.backgroundColor = UIColor(named: "priority")
            categoria.text = "Priority".uppercased()
        case "water":
            container.backgroundColor = UIColor(named: "water")
            categoria.text = "Water".uppercased()
        default:
            container.backgroundColor = UIColor(named: "daily")
            categoria.textColor = UIColor(named: "daily")
        }
        
        let stackView = UIStackView()
        
        for (index, activity) in task.activitys.enumerated() {
            let cbAtividade = UIButton()
            cbAtividade.tag = index
            cbAtividade.setTitle(activity.name, for: .normal)
            cbAtividade.titleLabel?.font = UIFont(name: "Roboto", size: 14)
            if(activity.isActive){
                cbAtividade.setImage(UIImage(systemName: "checkmark.square.fill"), for: .normal)
                senderCheckTrue(cbAtividade)
                cbAtividade.isSelected = true
            } else {
                cbAtividade.setImage(UIImage(systemName: "checkmark.square"), for: .normal)
                cbAtividade.tintColor = UIColor(named: "white")
                cbAtividade.isSelected = false
            }
            cbAtividade.addTarget(self, action: #selector(marcarAtividade), for: .touchUpInside)
            cbAtividade.setTitleColor(UIColor(named: "white"), for: .normal)
            cbAtividade.translatesAutoresizingMaskIntoConstraints = false

            stackView.addArrangedSubview(cbAtividade)
        }
        
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.spacing = 16
        stackView.distribution = .fill
        stackView.alignment = .leading
        self.scrollView.addSubview(stackView)
        self.scrollView.leadingAnchor.constraint(equalTo: stackView.leadingAnchor).isActive = true
        self.scrollView.trailingAnchor.constraint(equalTo: stackView.trailingAnchor).isActive = true
        self.scrollView.topAnchor.constraint(equalTo: stackView.topAnchor).isActive = true
        self.scrollView.bottomAnchor.constraint(equalTo: stackView.bottomAnchor).isActive = true
        self.scrollView.widthAnchor.constraint(equalTo: stackView.widthAnchor).isActive = true
    
    }
    
    @objc func marcarAtividade(_ sender: UIButton){
        if sender.isSelected {
            sender.setImage(UIImage(systemName: "checkmark.square"), for: .normal)
            sender.tintColor = UIColor(named: "white")
            sender.isSelected = false
            task.activitys[sender.tag].isActive = false
        } else {
            sender.setImage(UIImage(systemName: "checkmark.square.fill"), for: .normal)
            senderCheckTrue(sender)
            sender.isSelected = true
            task.activitys[sender.tag].isActive = true
        }
        
        atualizarTask()
    }
    
    func senderCheckTrue(_ sender: UIButton){
        switch task.category {
        case "daily":
            sender.tintColor = UIColor(named: "water")
        case "home":
            sender.tintColor = UIColor(named: "priority")
        case "priority":
            sender.tintColor = UIColor(named: "home")
        case "water":
            sender.tintColor = UIColor(named: "daily")
        default:
            sender.tintColor = UIColor(named: "daily")
        }
    }
    
    func atualizarTask(){
        let defaults = UserDefaults.standard
        var tasks = Array<Task>()
        if let taskData = defaults.object(forKey: "tasks") as? NSData {
            do {
                tasks = try NSKeyedUnarchiver.unarchiveTopLevelObjectWithData(taskData as Data) as! [Task]
            } catch {
                print("Um erro ocorreu ao recuperar o UserDefault")
            }
        }
        
        tasks[posicao] = task
        
        do {
            let encodedData: Data = try NSKeyedArchiver.archivedData(withRootObject: tasks, requiringSecureCoding: false)
            defaults.set(encodedData, forKey: "tasks")
            defaults.synchronize()
        } catch {
            print("Ocorreu um erro")
        }
    }
    
}
