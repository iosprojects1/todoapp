//
//  TableViewCell.swift
//  TodoApp
//
//  Created by aluno on 04/07/20.
//  Copyright © 2020 cesar. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var categoria: UILabel!
    @IBOutlet weak var atividade: UILabel!
    @IBOutlet weak var periodo: UILabel!
    @IBOutlet weak var editTask: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func prepare(with task: Task) {
        atividade.text = task.name
        periodo.text = task.taskDate
        
        switch task.category {
        case "daily":
            container.backgroundColor = UIColor(named: "daily")
            categoria.textColor = UIColor(named: "daily")
            categoria.text = "Daily".uppercased()
        case "home":
            container.backgroundColor = UIColor(named: "home")
            categoria.textColor = UIColor(named: "home")
            categoria.text = "Home".uppercased()
        case "priority":
            container.backgroundColor = UIColor(named: "priority")
            categoria.textColor = UIColor(named: "priority")
            categoria.text = "Priority".uppercased()
        case "water":
            container.backgroundColor = UIColor(named: "water")
            categoria.textColor = UIColor(named: "water")
            categoria.text = "Water".uppercased()
        default:
            container.backgroundColor = UIColor(named: "daily")
            categoria.textColor = UIColor(named: "daily")
        }
    }
    
}
